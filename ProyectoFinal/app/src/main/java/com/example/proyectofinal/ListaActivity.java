package com.example.proyectofinal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.proyectofinal.Objetos.Articulos;
import com.example.proyectofinal.Objetos.ReferenciasFirebase;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class ListaActivity extends AppCompatActivity {

  private FirebaseDatabase database;
  private DatabaseReference referencia;
  final Context context = this;
  private Articulos articulo;
  private ListView listView;
  private ArrayList<Articulos> copyArticulos = new ArrayList<>();
  private MyArrayAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lista);
    database = FirebaseDatabase.getInstance();
    referencia = database.getReferenceFromUrl(ReferenciasFirebase.URL_DATABASE
            + ReferenciasFirebase.DATABASE_NAME + "/" + ReferenciasFirebase.TABLE_NAME);
    listView = findViewById(R.id.list);
    obtenerArticulo();
  }

  public void obtenerArticulo(){
    final ArrayList<Articulos> articulos = new ArrayList<>();
    copyArticulos.clear();
    ChildEventListener listener = new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Articulos articulo = dataSnapshot.getValue(Articulos.class);
        articulos.add(articulo);
        copyArticulos.add(articulo);
        adapter = new MyArrayAdapter(context,R.layout.layout_articulos,articulos);
        listView.setAdapter(adapter);
      }
      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {
      }
      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {
      }
      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
      }
      @Override
      public void onCancelled(DatabaseError databaseError) {
      }
    };

    referencia.addChildEventListener(listener);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    obtenerArticulo();
  }

  class MyArrayAdapter extends ArrayAdapter<Articulos> implements View.OnClickListener{
    Context context;
    int textViewRecursoId;
    ArrayList<Articulos> objects;
    ArrayList<Articulos> copyObjects;
    public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Articulos> objects){
      super(context, textViewResourceId, objects);
      this.context = context;
      this.textViewRecursoId = textViewResourceId;
      this.objects = objects;
      this.copyObjects = objects;

      Log.e("hola", "MyArrayAdapter: " + this.objects.size() );
      Log.e("adios", "MyArrayAdapter: " + this.copyObjects.size() );
    }

    public View getView(final int position, View convertView, ViewGroup viewGroup){
      LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View view = layoutInflater.inflate(this.textViewRecursoId, null);
      CardView card_layout_id = view.findViewById(R.id.cv);
      ImageView imgsrc = (ImageView) view.findViewById(R.id.foto);
      TextView lblCodigo = (TextView)view.findViewById(R.id.txtCodigo);
      TextView lblDescripcion = (TextView)view.findViewById(R.id.txtDescripcion);
      lblCodigo.setText(objects.get(position).getCodigo());
      lblDescripcion.setText(objects.get(position).getDescripcion());
      Picasso.get().load(Uri.parse(objects.get(position).getImagen())).into(imgsrc);

      card_layout_id.setOnClickListener(v -> {
        articulo = objects.get(position);
        Log.e("xxxxxxxxxxxxxxxxxxxxxx", "onClick: asd" + objects.size());
        Intent intent = new Intent(ListaActivity.this, HomeActivity.class);
        Bundle datos = new Bundle();
        datos.putSerializable("producto",objects.get(position));
        intent.putExtra("posicion", position);
        intent.putExtras(datos);
        Log.e("sdoiufdfgnoi", "onClick: asd" + datos);
        startActivityForResult(intent, 2);
      });
      return view;
    }

    @Override
    public void onClick(View v) {

    }

    public void update(String newText){
      objects.clear();

      Log.e("adioss", "MyArrayAdapter: " + copyObjects.size() );
      if(newText.isEmpty()){
        objects.addAll(copyArticulos);
        Log.e("holaas", "MyArrayAdapter: " + objects.size() );
      } else {
        for (Articulos item : copyArticulos){
          if(item.getCodigo().toLowerCase(Locale.getDefault()).contains(newText.toLowerCase(Locale.getDefault()))){
            objects.add(item);
          }
        }
      }
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.menuitem,menu);
    MenuItem menuItem = menu.findItem(R.id.menu_search);
    SearchView searchView = (SearchView) menuItem.getActionView();

    // Codificar los metodos para la busqueda
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        return false;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        Log.e("TAG", "onQueryTextChange: entra" );
        ListaActivity.this.adapter.update(newText);
        ListaActivity.this.adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);

        return false;
      }
    });
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.cerrarSesion) {
      Toast.makeText(this, "Sesión cerrada", Toast.LENGTH_SHORT).show();
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }
}
