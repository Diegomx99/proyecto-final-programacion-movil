package com.example.proyectofinal.Objetos;

import android.net.Uri;

import java.io.Serializable;

public class Articulos implements Serializable {

    private String _ID;
    private String codigo;
    private String descripcion;
    private String imagen;

    public Articulos(){
    }

    public Articulos(String _ID, String codigo, String descripcion, String imagen) {
        this._ID = _ID;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
