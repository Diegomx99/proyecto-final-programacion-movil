package com.example.proyectofinal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AuthActivity extends Activity {
  private Button btnAcceder;
  private EditText txtCorreo, txtPassword;
  private FirebaseAuth mAuth;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_auth);

    this.initComponentes();

    this.btnAcceder.setOnClickListener(v -> this.handleSignIn());
  }

  private void initComponentes() {
    this.mAuth = FirebaseAuth.getInstance();

    this.btnAcceder = findViewById(R.id.btnAcceder);
    this.txtCorreo = findViewById(R.id.txtCorreo);
    this.txtPassword = findViewById(R.id.txtPassword);
  }

  private void handleSignIn() {
    // Verifica si está conectado a internet
    if (this.isNetworkAvailable()) {
      // Valida si algún campo está vacío
      if (this.isSomeInputEmpty()) {
        this.createToast("Rellena todos los campos");
      } else {
        this.accederCuenta();
      }
    }
  }

  private void createToast(String mensaje) {
    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
  }

  private boolean isSomeInputEmpty() {
    return this.txtCorreo.getText().toString().equals("") ||
      this.txtPassword.getText().toString().equals("");
  }

  public void accederCuenta() {
    this.mAuth.signInWithEmailAndPassword(
      this.txtCorreo.getText().toString(),
      this.txtPassword.getText().toString()
    ).addOnCompleteListener(this, task -> {
        if (task.isSuccessful()) {
          // Sign in success, update UI with the signed-in user's information
          FirebaseUser user = mAuth.getCurrentUser();
          updateUI(user);
          Intent i = new Intent(AuthActivity.this,ListaActivity.class);
          startActivityForResult(i,0);
          this.cleanUp();
        } else {
          // If sign in fails, display a message to the user.
          this.createToast("La Autenticación falló");
          updateUI(null);
        }
    });
  }

  public boolean isNetworkAvailable() {
    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo ni = cm.getActiveNetworkInfo();
    return ni != null && ni.isConnected();
  }

  private void updateUI(FirebaseUser user) {

  }

  private void cleanUp() {
    this.txtCorreo.setText("");
    this.txtPassword.setText("");
  }
}
