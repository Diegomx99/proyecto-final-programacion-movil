package com.example.proyectofinal.handlers;

import com.example.proyectofinal.Objetos.Articulos;
import com.google.firebase.database.DatabaseReference;

public class FirebaseHandler {
  private DatabaseReference referencia;

  public FirebaseHandler(DatabaseReference _referencia) {
    this.referencia = _referencia;
  }

  public void insert(Articulos articulo) {
    String id = this.referencia.push().getKey();
    articulo.set_ID(id);
    this.referencia.child(id).setValue(articulo);
  }

  public void update(String id, Articulos articulo) {
    articulo.set_ID(id);
    this.referencia.child(String.valueOf(id)).setValue(articulo);
  }

  public void delete(String id) {
    this.referencia.child(id).removeValue();
  }

}
