package com.example.proyectofinal;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.proyectofinal.Objetos.Articulos;
import com.example.proyectofinal.Objetos.ReferenciasFirebase;
import com.example.proyectofinal.handlers.FirebaseHandler;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class HomeActivity extends Activity implements View.OnClickListener {
  String uid;
  EditText txtCodigo, txtDescripcion;
  TextView lblUrl;
  Button btnImagen, btnRegresar, btnBorrar, btnGuardar;
  private ImageView imgArticulo;
  private Uri selectedImgUri;
  private FirebaseDatabase database;
  private DatabaseReference referencia;
  private Articulos savedContacto;
  private StorageReference mStorageRef;
  private StorageTask<UploadTask.TaskSnapshot> mUploadTask;
  private String thumb_download_url , id;

  private FirebaseHandler firebase;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);

    this.initComponentes();
    this.getUserProfile();
    this.setEvents();

    Bundle oBundle = getIntent().getExtras();
    if (oBundle != null) {
      this.initBundle(oBundle);
    }
  }

  private void initComponentes() {
    this.txtCodigo = findViewById(R.id.txtCodigo);
    this.txtDescripcion = findViewById(R.id.txtDescripcion);
    this.lblUrl = findViewById(R.id.lblUrl);
    this.btnImagen = findViewById(R.id.btnImagen);
    this.btnRegresar = findViewById(R.id.btnRegresar);
    this.btnBorrar = findViewById(R.id.btnBorrar);
    this.btnGuardar = findViewById(R.id.btnGuardar);
    this.imgArticulo = findViewById(R.id.imgArticulo);

    this.database = FirebaseDatabase.getInstance();
    this.mStorageRef = FirebaseStorage.getInstance().getReference();
    this.referencia = this.database.getReferenceFromUrl(
      ReferenciasFirebase.URL_DATABASE +
      ReferenciasFirebase.DATABASE_NAME + "/" + ReferenciasFirebase.TABLE_NAME
    );

    this.savedContacto = null;
    this.selectedImgUri = null;

    this.firebase = new FirebaseHandler(this.referencia);
  }

  private void initBundle(Bundle oBundle) {
    Articulos articulo = (Articulos) oBundle.getSerializable("producto");
    this.savedContacto = articulo;
    this.id = articulo.get_ID();
    this.txtCodigo.setText(articulo.getCodigo());
    this.txtDescripcion.setText(articulo.getDescripcion());
    this.lblUrl.setText(articulo.getImagen());
    Picasso.get().load(Uri.parse(articulo.getImagen())).into(imgArticulo);
  }

  public void setEvents() {
    this.btnImagen.setOnClickListener(this);
    this.btnRegresar.setOnClickListener(this);
    this.btnBorrar.setOnClickListener(this);
    this.btnGuardar.setOnClickListener(this);
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btnImagen:
        openFileChooser();
        break;
      case R.id.btnRegresar:
        setResult(Activity.RESULT_OK);
        finish();
        break;
      case R.id.btnBorrar:
        borrarArticulo(id);
        setResult(Activity.RESULT_OK);
        finish();
        break;
      case R.id.btnGuardar:
        Articulos nArticulo = new Articulos();
        nArticulo.setCodigo(txtCodigo.getText().toString());
        nArticulo.setDescripcion(txtDescripcion.getText().toString());
        actualizarArticulo(id,nArticulo);
        break;
    }
  }

  private void openFileChooser() {
    Intent intent = new Intent();
    intent.setType("image/*");
    intent.setAction(Intent.ACTION_GET_CONTENT);
    startActivityForResult(Intent.createChooser(intent, "Select Picture"),0);
  }

  public void actualizarArticulo(String id, Articulos articulo) {
    if (selectedImgUri != null) {

      StorageReference filePath = mStorageRef.child("pics").child(selectedImgUri.getLastPathSegment());
      mUploadTask = filePath.putFile(selectedImgUri).addOnSuccessListener(taskSnapshot ->
        mUploadTask.continueWithTask(task -> {
          if (!task.isSuccessful()) throw task.getException();

          // Continue with the task to get the download URL
          return filePath.getDownloadUrl();
        }).addOnCompleteListener(task -> {
          if (task.isSuccessful()) {
            // Guardar url de la imagen
            thumb_download_url = task.getResult().toString();
            lblUrl.setText(thumb_download_url);
            articulo.setImagen(thumb_download_url);

            // Actualizar en bd
            this.firebase.update(id,articulo);

            setResult(Activity.RESULT_OK);
            finish();
          }
        })
      );
    } else {
      articulo.setImagen(lblUrl.getText().toString());

      // Actualizar en bd
      this.firebase.update(id, articulo);

      setResult(Activity.RESULT_OK);
      finish();
    }
  }

  public void borrarArticulo(String id) {
    this.firebase.delete(id);
  }

  public void getUserProfile() {
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    if (user != null) {
      uid = user.getUid();
    } else {
      Intent i = new Intent(HomeActivity.this,AuthActivity.class);
      startActivityForResult(i,0);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == 0) {
      selectedImgUri = data.getData();
      imgArticulo.setImageURI(Uri.parse(selectedImgUri.toString()));
    }
  }
}
