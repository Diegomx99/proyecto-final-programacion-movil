const theme = {
    fondo: '#F9F9F9',
    colorPrimario: '#ff90b5',
    colorPrimarioHover: '#e86c95',
    colorSecundario: '#ff90b552',
    verde: '#43A854',
    rojo: '#E34747'
}

export default theme;